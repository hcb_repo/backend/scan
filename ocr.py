# import the necessary packages
from PIL import Image
import pytesseract
import argparse
import cv2
import os
# construct the argument parse and parse the arguments
#
# ap = argparse.ArgumentParser()
# ap.add_argument("-i", "--image", required=True,
# 	help="path to input image to be OCR'd")
# ap.add_argument("-p", "--preprocess", type=str, default="thresh",
# 	help="type of preprocessing to be done")
# args = vars(ap.parse_args())
#
#
# image = cv2.imread(args["image"])
# image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
# # use Tesseract to OCR the image
# text = pytesseract.image_to_string(image, lang="rus")
# print(text)

def extractText(image):
	pytesseract.pytesseract.tesseract_cmd = 'C:\\Program Files\\Tesseract-OCR\\tesseract.exe'
	text = pytesseract.image_to_string(image, lang="rus")
	return text