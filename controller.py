import cv2
import numpy as np
from flask import Flask, flash, request, redirect, url_for
app = Flask(__name__)

from ocr import extractText

@app.route("/scan-img", methods = ['POST'])
def hello_world():
  print("POST")
  f = request.files['file'].read()
  image = cv2.imdecode(np.fromstring(f, dtype=np.uint8), cv2.IMREAD_COLOR)
  text = extractText(image)
  return text
